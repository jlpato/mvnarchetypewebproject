#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false" %>
<html>
<body>
<h1>${symbol_dollar}{msg}</h1>
<h2>Today is <fmt:formatDate value="${symbol_dollar}{today}" pattern="yyy-MM-dd" /></h2>
</body>
</html>
