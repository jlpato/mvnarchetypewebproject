<h1>Spring MVC Quickstart Maven Archetype</h1>

<h2>The project is a Maven archetype for Spring MVC web application.</h2>

<h3>Install archetype locally</h3>

To install the archetype in your local repository download the zip file, extract and run:

    cd mvnarchetypewebproject-master
    mvn clean install

Create a project from a local repository

Create a new empty directory for your project and navigate into it and then run:

    mvn archetype:generate \
        -DarchetypeGroupId=pl.codeleak \
        -DarchetypeArtifactId=spring-mvc-quickstart \
        -DarchetypeVersion=5.0.1 \
        -DgroupId=my.groupid \
        -DartifactId=my-artifactId \
        -Dversion=version
